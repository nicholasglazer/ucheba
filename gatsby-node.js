const path = require(`path`)
/* const { createFilePath } = require(`gatsby-source-filesystem`) */
const { createUniversities, createDirections, createCollegesSearch, createProfCollegesSearch, createSpecializations, createUniversitiesSearch, createSearch } = require(`./src/gatsby/pageCreator`)
const gatsbyNodeGraphQL = require('./src/gatsby/gatsbyNodeGraphQL')
const cyrillicToTranslit = require(`cyrillic-to-translit-js`)

const wrapper = promise =>
      promise.then(result => {
        if (result.errors) {
          throw result.errors
        }
        return result
      })

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  const short = node.university_short_name
  const full = node.university_name
  if (node.internal.type === `UniversitiesJson`) {
    let name = short === '' || short === '.' || short === '-' ? full : short
    let universityName = cyrillicToTranslit({ preset: "uk" }).transform(`${name}`, "_").toLowerCase()
    createNodeField({
      node,
      name: `slug`,
      value: universityName,
    })
  }
  if (node.internal.type === `DirectionsJson`) {
    let directionName = cyrillicToTranslit({ preset: "uk" }).transform(`${node.category_name}`, "_").toLowerCase()

    const linksArray = []
    createNodeField({
      node,
      name: `slug`,
      value: directionName,
    })
    console.log('internal type array', linksArray)
    node.categories.forEach((sc) => {
      console.log('sc slug value', sc.link)
      linksArray.push(sc.link)
    })
    createNodeField({
      node,
      name: `slugsArray`,
      value: linksArray,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const searchTemplate = require.resolve('./src/templates/searchTemplate.js')
  const specializationTemplate = require.resolve('./src/templates/specializationTemplate.js')
  const directionTemplate = require.resolve('./src/templates/direction.js')
  const universityTemplate = require.resolve('./src/templates/university.js')
  const collegesSearchTemplate = require.resolve('./src/templates/collegesSearchTemplate.js')
  const profCollegesSearchTemplate = require.resolve('./src/templates/profCollegesSearchTemplate.js')
  const universitiesSearchTemplate = require.resolve('./src/templates/universitiesSearchTemplate.js')

  const result = await wrapper (
    graphql(`
    {
      ${gatsbyNodeGraphQL}
    }
  `)
  )

  createUniversities(result.data.universities.edges, createPage, universityTemplate)
  createDirections(result.data.directions.edges, createPage, directionTemplate)
  createSpecializations(result.data.directions.edges, createPage, specializationTemplate)
  createSearch(result.data.universities.edges, createPage, searchTemplate)
  createCollegesSearch(result.data.universities.edges, createPage, collegesSearchTemplate)
  createProfCollegesSearch(result.data.colleges.edges, createPage, profCollegesSearchTemplate)
  createUniversitiesSearch(result.data.universities.edges, createPage, universitiesSearchTemplate)
}
