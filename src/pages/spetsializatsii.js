import React from "react"
import { graphql, Link } from 'gatsby'
import Layout from "../components/layout"
import Hero from '../components/hero'
import SEO from "../components/seo"
import SpecializationList from "../components/specializationList"
import { FormattedMessage, injectIntl } from "gatsby-plugin-intl"

const ProfessionsMore = ({ data, intl, pageContext, location }) => {
  const directions = data.allDirectionsJson.edges
  return (
    <Layout>
      <SEO
        title={intl.formatMessage({ id: "title" })}
        keywords={[`gatsby`, `application`, `react`]}
      />
      <Hero>
      </Hero>
      <div className="container">
        <SpecializationList data={directions} />
        <FormattedMessage id="learn_more" />
      </div>
    </Layout>
  )
}

export default injectIntl(ProfessionsMore)

export const query = graphql`
  query {
    allDirectionsJson {
      edges {
        node {
          category_name
          categories {
            id
            name
            link
            image
            description
          }
        }
      }
    }
  }
`
/* import React from 'react'
 * import { graphql } from 'gatsby'
 * import { injectIntl } from "gatsby-plugin-intl"
 * import Layout from '../components/layout'
 * import SEO from '../components/seo'
 * import Hero from '../components/hero'
 * 
 * class AllDirections extends React.Component {
 *   constructor(props) {
 *     super(props)
 *     this.state = {}
 *     this.data = props.data
 *   }
 *   collectUniversities() {
 *     console.log('directions', this.data.directionsJson)
 *     const directions = this.data.directionsJson
 *     const universities = this.data.allUniversitiesJson
 * 
 *     const cmap = directions.reduce((map, c) => {
 *       c.categories.forEach(sc => {
 *         map.set(sc.id, sc)
 *       })
 *       return map
 *     }, new Map())
 *     const obj = universities.forEach(u => {
 *       u.speciality_licenses.forEach(s => {
 *         const c = cmap.get(s.speciality_code)
 *         if (c) {
 *           if (!c.universities) {
 *             c.universities = []
 *             c.universities.push(u)
 *           }
 *         }
 *         return c
 *       })
 *     })
 *     console.log('obj', obj)
 *     return null
 * 
 *   }
 *   render() {
 *     const d = this.data.directionsJson
 *     this.collectUniversities()
 *     return (
 *       <Layout>
 *         <SEO
 *           title={`напрямки`}
 *           description={d.category_description}
 *           keywords={[d.category_name]}
 *         />
 *         <Hero />
 *         <div className="container">
 *           <div>{d.category_name}</div>
 *           <div>{d.category_description}</div>
 *           {d.categories.map((c, i) => (<div key={i}>{c.id}{c.name}</div>))}
 * 
 *         </div>
 *       </Layout>
 *     )
 *   }
 * }
 * 
 * export const query = graphql`
 *   query($slug: String!) {
 *     directionsJson(fields: { slug: { eq: $slug } }) {
 *       ...DirectionsProps
 *     }
 *     allUniversitiesJson {
 *       edges {
 *         node {
 *           speciality_licenses {
 *             speciality_code
 *           }
 *         }
 *       }
 *     }
 *   }
 * `
 * 
 * export const allDirectionsJsonFragment = graphql`
 *   fragment DirectionsProps on DirectionsJson {
 *     category_id
 *     category_name
 *     category_description
 *     categories {
 *       id
 *       name
 *       description
 *     }
 *   }
 * `
 * 
 * export default injectIntl(AllDirections) */
