import React from "react"
import { FormattedMessage, injectIntl } from "gatsby-plugin-intl"
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import Hero from '../components/hero'
import SEO from "../components/seo"
import DirectionCard from "../components/directionCard"
import s from '@emotion/styled'

const IndexPage = ({ data, intl, pageContext, location }) => {
  const directions = data.allDirectionsJson.edges
  return (
    <Layout>
      <SEO
        title={intl.formatMessage({ id: "title" })}
        keywords={[`gatsby`, `application`, `react`]}
      />
      <Hero />
      <MainSection className="section">

          <DirectionCard data={directions} />
      </MainSection>
    </Layout>
  )
}

const MainSection = s.section`

`
export default injectIntl(IndexPage)

export const query = graphql`
  query {
    allDirectionsJson {
      edges {
        node {
          category_id
          category_name
          category_description
          category_icon
          categories {
            id
            name
            description
          }
        }
      }
    }
  }
`
