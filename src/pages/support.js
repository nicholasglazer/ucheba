import React from "react"
/* import { graphql } from 'gatsby' */
import Layout from "../components/layout"
import Hero from '../components/hero'
import SEO from "../components/seo"
import { FormattedMessage, injectIntl } from "gatsby-plugin-intl"

const Support = ({ intl, pageContext }) => {
  return (
    <Layout>
      <SEO
        title={intl.formatMessage({ id: "title" })}
        keywords={[`gatsby`, `application`, `react`]}
      />
      <Hero>
      </Hero>
      <div className="container">
        <FormattedMessage id="footer_disclamerSupport" />
      </div>
    </Layout>
  )
}

export default injectIntl(Support)
