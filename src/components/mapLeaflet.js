import React, { Component } from 'react'
import { Map, TileLayer, Marker, Popup } from "react-leaflet"
import LCG from "leaflet-control-geocoder"
import L from "leaflet"

export default class LeafletMap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lat: '51.5',
      lng: '0.12',
      zoom: 18,
    }
    this.data = props.data
  }

  componentDidMount() {
    const geocoder = LCG.L.Control.Geocoder.nominatim();
    const address = this.data.address.substring(this.data.address.indexOf(".") + 1);
    console.log('location full', `${this.data.region}, ${this.data.index}, ${address}`)
    geocoder.geocode(`${this.data.region}, ${this.data.index}, ${address}`, (results) => {
      console.log('res', results)
      const r = results[0].properties
      this.setState({lat: r.lat, lng: r.lon})
    })
  }
  render() {
    if (typeof window !== 'undefined') {
      const position = [this.state.lat, this.state.lng]
      const geocoder = LCG.L.Control.Geocoder.nominatim();
      return (
        <Map center={position} zoom={this.state.zoom}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </Map>
      )
    }
    return null
  }
}
/*  NB! Leaflet makes direct calls to the DOM when it is loaded, therefore this library is not compatible with server-side rendering. */
/* import LCG from "leaflet-control-geocoder/dist/Control.Geocoder.js"; */
/* 
 * import React, { Component } from "react"
 * import L from "leaflet"
 * import LCG from "leaflet-control-geocoder"
 * 
 * 
 * export default class MapLeaflet extends Component {
 *   constructor() {
 *     super()
 *     this.state = {
 *       center: [lat: 51.5, lng: 0.12];
 *       height: "100vh"
 *     }
 *   }
 *   componentDidMount() {
 *     const map = this.leafletMap.leafletElement;
 *     const geocoder = LCG.L.Control.Geocoder.nominatim();
 *     let marker;
 * 
 *     map.on("click", e => {
 *       geocoder.reverse(
 *         e.latlng,
 *         map.options.crs.scale(map.getZoom()),
 *         results => {
 *           var r = results[0];
 *           if (r) {
 *             if (marker) {
 *               marker
 *                 .setLatLng(r.center)
 *                 .setPopupContent(r.html || r.name)
 *                 .openPopup();
 *             } else {
 *               marker = L.marker(r.center)
 *                 .bindPopup(r.name)
 *                 .addTo(map)
 *                 .openPopup();
 *             }
 *           }
 *         }
 *       );
 *     });
 *   }
 * 
 *   render() {
 *     return (
 *       <Map
 *         style={this.state.height}
 *         center={this.state.center}
 *         zoom={18}
 *         ref={m => {
 *           this.leafletMap = m;
 *         }}
 *       >
 *         <TileLayer
 *           url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
 *           tms={true}
 *           attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
 *         />
 *       </Map>
 *     );
 *   }
 * }
 *  */
