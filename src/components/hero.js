import React from 'react'
import s from '@emotion/styled'

const Hero = (props) => <Wrapper style={{background: props.bg ? props.bg : '#fff' }}>{props.children}</Wrapper>

const Wrapper = s.div`
padding: 148px 0 0 0;
`

export default Hero
