import React from "react"
import s from '@emotion/styled'
import { css } from '@emotion/core'
import { FormattedMessage, injectIntl } from "gatsby-plugin-intl"
import VnzTab from '../components/vnzTab.js'

const VnzTabs = props => {
  const { data, tabList, activeTab, changeActiveTab } = props
  return (
    <Tabs style={{marginBottom: '0'}} className="tabs is-boxed">
      <ul>
        {tabList.map(tab =>
          <VnzTab
            tab={tab}
            key={tab.name}
            activeTab={activeTab}
            changeActiveTab={changeActiveTab}
          />
        )}
      </ul>
    </Tabs>
  )
}

const Tabs = s.div`
marginBottom: 0;
padding-top: .6rem;
padding-left: .6rem;
background: #f4f4f4;
`

export default VnzTabs

/* <li className="is-active">
 * <a>
 * <span className="icon is-small"><i className="fas fa-image" aria-hidden="true"></i></span>
 * <span>Про ВНЗ</span>
 * </a>
 * </li>
 * <li>
 * <a>
 * <span className="icon is-small"><i className="fas fa-music" aria-hidden="true"></i></span>
 * <span>Educators</span>
 * </a>
 * </li>
 * <li>
 * <a>
 * <span className="icon is-small"><i className="fas fa-film" aria-hidden="true"></i></span>
 * <span>Specialities</span>
 * </a>
 * </li>
 * <li>
 * <a>
 * <span className="icon is-small"><i className="far fa-file-alt" aria-hidden="true"></i></span>
 * <span>Else</span>
 * </a>
 * </li> */
