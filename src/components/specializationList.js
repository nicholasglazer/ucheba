import React from "react"
import SpecializationCard from "../components/specializationCard"
import s from '@emotion/styled'

const SpecializationList = ({data}) => {
  console.log('datatttat@', data)
  return (
    <SpecializationsWrapper>
      {data.map(({node}, i) => {
         return (
           <div key={i}>
             <div className="title">
               {node.category_name}
             </div>
             <Specializations>
               {node.categories.map((s,j) =>
                 <SpecializationCard key={j} data={s} />
               )}
             </Specializations>
           </div>
         )
      })}
    </SpecializationsWrapper>
  )
}


const Specializations = s.div`

`
const SpecializationsWrapper = s.div`

`

export default SpecializationList
