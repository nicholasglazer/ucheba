import React from "react"
import s from '@emotion/styled'

const VnzTabs = props => {
  const { data } = props
  return (
    <MapSection>
      <div>
        {data}
      </div>
    </MapSection>
  )
}


const MapSection = s.section`
padding: 3rem;
margin-bottom: 1rem;
`

export default VnzTabs
