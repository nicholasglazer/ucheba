import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import { injectIntl } from "gatsby-plugin-intl"
import s from '@emotion/styled'

import Header from "./header"
import Footer from "./footer"
import "./layout.scss"

const Layout = (props) => (
  <StaticQuery
  query={graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `}
    render={data => {
    return (
    <>
        <Header siteTitle={props.intl.formatMessage({ id: "title" })} />
        <MainWrapper>
          <main>{props.children}</main>
        </MainWrapper>
        <Footer />
    </>
  )}}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}
const MainWrapper = s.div`
`

export default injectIntl(Layout)
