import { injectIntl, Link, FormattedMessage } from "gatsby-plugin-intl"
import { graphql, StaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
/* import Language from "./language" */
/* import s from '@emotion/styled' */
import { Location } from '@reach/router'
import {css} from '@emotion/core'
import { MdSearch } from 'react-icons/md'
import BackHistoryButton from '../components/backHistoryButton'

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.burger = React.createRef()
    this.menu = React.createRef()
    this.props = props
  }

  toggle = () => {
    if (this.menu.current.classList.contains("is-active")) {
      this.burger.current.classList.remove("is-active");
      this.menu.current.classList.remove("is-active");
    } else {
      this.burger.current.classList.add("is-active");
      this.menu.current.classList.add("is-active");
    }
  };
  render() {
    return (
      <>
      <StaticQuery
      query={graphql`
        query {
          allUniversitiesJson {
            edges {
              node {
                ...UniversitiesProps
              }
            }
          }
        }
      `}
      render={data => (
        <Location>
        {({ location }) => {
          return (
            <nav css={nav} className="navbar is-transparent section" role="navigation" aria-label="main navigation">
              <div className="container">
                <div className="navbar-brand" css={navBrand}>
                  { location.pathname === '/' ? <Link to={'/'}>Ucheba.ua</Link> :
                    (<div>
                      <BackHistoryButton size={20} />
                    </div>)
                  }
                  <div ref={this.burger} onClick={this.toggle} className="navbar-burger burger" role="button" aria-label="menu" aria-expanded="false" data-target="NavBar">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                  </div>
                </div>
                <div ref={this.menu} id="NavBar" className="navbar-menu">
                  <div className="navbar-start">

                    { location.pathname !== '/' ? (
                    <Link to={'/'} className="navbar-item" style={{paddingLeft: '4rem'}}>
                      <FormattedMessage id="main" />
                    </Link>) : null }
                  </div>
                  <div className="navbar-end">
                    <Link to="/universities" className="navbar-item">
                      <FormattedMessage id="universities" />
                    </Link>
                    <Link to="/colleges" className="navbar-item">
                      <FormattedMessage id="colleges" />
                    </Link>
                    <Link to="/contacts" className="navbar-item">
                      <FormattedMessage id="contacts" />
                    </Link>
                    <Link to="/search" className="navbar-item">
                      <MdSearch style={{fontSize: '20px'}}/>
                    </Link>
                  </div>
                </div>
              </div>
            </nav>
          )}
        }
       </Location>
      )}
      />
      </>
    )
  }
}

const navAbsolute =css`
width: 100%;
`
const nav = css`
font-weight: 500;
position: absolute;
width: 100%;
`
const navBrand = css`
align-items: center;
padding-left: 10px;
@media (max-width: 767px) {
}
`

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default injectIntl(Header)

/* width: 100%; */
// Language module

/* <FormattedMessage id="search" /> */

/* <NavSection>
 * </NavSection> */
/* display: flex;
 * align-items: center;
 * justify-content: center;
 * width: 100%;
 * const NavSection = s.section`
 * position: absolute;
 * top: 0px;
 * left: 0px;
 * right: 0px;
 * z-index: 1000;
 * ` */
/* const padR = css({paddingRight: '14px'}) */

/* display: flex;
 * width: 100%;
 * justify-content: space-between;
 * align-items: center;
 * height: 140px;
 * top: 0px; */
