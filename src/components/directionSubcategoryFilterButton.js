import React from 'react'
import s from '@emotion/styled'
import {css} from '@emotion/core'

const SubcategoryFilterButton = props => {
  const { data, setSubcategory, selected } = props
  return (
    <section css={W}>
      <div className="container">
        <div className="columns is-centered is-multiline is-mobile">
          {data.map((c, i) => {
             return (
               <SubcategoryCard key={i} style={{color: `${selected === c.id ? '#fff' : '#383838'}`}} onClick={() => setSubcategory({code: c.id, name: c.name})} className={`${selected === c.id ? 'is-active-subcategory' : ''} column `}>
                 <div style={{textAlign: 'center'}}>
                   {c.name}
                 </div>
               </SubcategoryCard>
             )}
          )}
        </div>
      </div>
    </section>
  )
}

const W = css`
 box-shadow: inset 0px 2px 4px rgba(0,0,0, .32);
 padding: 4rem;
 background: #eee;
`
const SubcategoryCard = s.div`
 font-size: .9rem;
 font-weight: 500;
 text-transform: uppercase;
 background: white;
 border-left: 3px solid #50c878;
 padding: 1rem;
 min-width: 576px;
 max-width: 576px;
 margin: 6px;
 transition: all .23s ease;
 box-shadow: 2px 2px 4px rgba(0,0,0, .12);
 @media(max-width: 700px) {
 min-width: 250px;
 }
 & :hover {
 background: #50c878;
 cursor: pointer;
 box-shadow: inset 1px 1px 10px rgba(0,0,0, .1);
 color: #fff !important;
 }
 & :active {
   box-shadow: inset 1px 1px 12px rgba(0,0,0, .18) !important;
   color: #777 !important;
 }
`

export default SubcategoryFilterButton
