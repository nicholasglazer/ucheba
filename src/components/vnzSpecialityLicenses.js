import React from 'react'
/* import s from '@emotion/styled' */
import Card from '../components/directionSubcategoryCard'

const VnzSpecialityLicenses = props => {
  const { data } = props
  console.log('data', data)
  return (
    <Card filteredResults={data.speciality_licenses}/>
  )
}

export default VnzSpecialityLicenses

/* <div>
 * <div>
 * Speciality Licenses
 * </div>
 * <div>
 * {data.speciality_licenses ? data.speciality_licenses.map((y, i) => {
 *   return (
 *     <div key={i} className="card">
 *       <div className="card-content">
 *         <div>qualification_group_name{y.qualification_group_name}</div>
 *         <div>speciality_code{y.speciality_code}</div>
 *         <div>speciality_name{y.speciality_name}</div>
 *         <div>specialization_name{y.specialization_name}</div>
 *         <div>full_time_count{y.full_time_count}</div>
 *         <div>part_time_count{y.part_time_count}</div>
 *         <div>evening_count{y.evening_count}</div>
 *         <div>certificate{y.certificate}</div>
 *         <div>certificate_expired{y.certificate_expired}</div>
 *       </div>
 *     </div>
 *   )
 * }
 * ) : null}
 * </div>
 * </div> */
