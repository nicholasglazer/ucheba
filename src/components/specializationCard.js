import React from "react"
import { Link } from 'gatsby'
import s from '@emotion/styled'
import { css } from '@emotion/core'
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import { FormattedMessage, injectIntl } from 'gatsby-plugin-intl'
import { MdChevronRight } from 'react-icons/md'

const SpecializationCard = ({ data }) => {
  const specializations = []
  return (
    <Link to={`/spetsializatsiya/${data.link}`}>
      <SpecializationCardWrapper className="container">
        {data.name}
      </SpecializationCardWrapper>
    </Link>
  )
}



const Cards = s.div`

`
const Card = s.div`
height: 7.775rem;
width: 14.375rem;
padding: 1em;
border: 1px solid rgba(144,144,144, .33);
display: flex;
word-wrap: wrap;
vertical-align: center;
position: relative;
transition: box-shadow: .4s ease;
transition: fill: .4s ease;
  :hover {
    box-shadow: 0px 0px 18px 1px rgba(1,1,1, .2);
    color: #50c878 !important;
    .direction-icon-wrapper > svg {
      fill: #50c878;
    }
  }
  :active {
    box-shadow: inset 0px 0px 18px 1px rgba(1,1,1, .09);
  }
  .direction-icon-wrapper > svg {
    position: absolute;
    bottom: 2%;
    right: 2%;
    fill: #dbdbdb;
  }
`
const lastCard = css`
background: #f5f5f5;
max-width: 690px;
@media (max-width: 982px) {
min-width: 230px;
max-width: 230px;
}
            :hover {
              color: #50c878;
            }
`
const linkCard = css`
flex-direction: column;
justify-content: space-between;
display: flex;
width: 100%;
transition: color .4s ease;
            :hover {
              color: #50c878;
            }

`
const CardDirectionTitle = s.div`
font-weight: 600;
font-size: .85rem;
`
const CardDirectionQuantity = s.div`
font-weight: 500;
font-size: .85em;
display: flex;
div {
  flex: 1;
}
`

const SpecializationCardWrapper = s.div`
`
const SectionTitle = s.div`
border-left: 12px solid rgba(144,144,144, .7);
padding-left: 12px;
`

export default injectIntl(SpecializationCard)

/* <SectionTitle className="title is-4">
 *   <FormattedMessage id="direction_mainpage_title" />
 * </SectionTitle>
 * <Cards className="columns is-mobile is-multiline is-centered is-marginless">
 *   {data.map(({node}, i) => {
 *      specializations.push(node.category_name)
 *      const link = cyrillicToTranslit({ preset: "uk" }).transform(`${node.category_name}`, "_").toLowerCase()
 *      let Icon = Map[node.category_icon]
 *      return (
 *        <Card key={i} className="column is-narrow is-mobile is-centered">
 *          <Link css={linkCard} to={`/${link}`}>
 *            <div className="direction-icon-wrapper">
 *              <Icon size={60} />
 *            </div>
 *              <CardDirectionTitle>
 *                {node.category_name.toUpperCase()}
 *              </CardDirectionTitle>
 *              <CardDirectionQuantity>
 *                <p style={{marginRight: 'auto'}}>
 *                  <span style={{paddingRight: '6px'}}>
 *                    {node.categories.length}
 *                  </span>
 *                  {node.categories.length === 1 ? (
 *                     <FormattedMessage id="direction_card_quantity_single" />
 *                  ) : node.categories.length >= 5 ? (
 *                     <FormattedMessage id="direction_card_quantity_plural" />
 *                  ): (
 *                     <FormattedMessage id="direction_card_quantity_multiple" />
 *                  )}
 *                </p>
 *              </CardDirectionQuantity>
 *          </Link>
 *        </Card>
 *      )
 *   })}
 *   <Card css={lastCard} className="column is-mobile is-multiline is-centered">
 *     <Link css={linkCard}
 *           to={`/professions`}
 *           state={specializations}
 *     >
 *       <div></div>
 *       <CardDirectionTitle>
 *         <FormattedMessage id="learn_more" />
 *       </CardDirectionTitle>
 *       <CardDirectionQuantity style={{ marginLeft: 'auto' }}>
 *         <MdChevronRight size={20} />
 *       </CardDirectionQuantity>
 *     </Link>
 *   </Card>
 * </Cards> */
