import React from 'react'
import s from '@emotion/styled'
import { FormattedMessage, injectIntl } from 'gatsby-plugin-intl'

const DirectionFilterButton = props => {
  const { resetFilter } = props
  return (
    <ResetFilterButton onClick={() => resetFilter()} className="button is-small is-fullwidth">
      <FormattedMessage id="direction_resetFilter" />
    </ResetFilterButton>
  )
}

const ResetFilterButton = s.button`
max-width: 7rem;
margin: 0;
cursor: pointer;
border: none;
color: #63b5cf !important;
font-weight: 500;
              :hover {
                background: #63b5cf !important;
                color: white !important;
              }
`

export default injectIntl(DirectionFilterButton)
