import React from "react"
import s from '@emotion/styled'
import { css } from '@emotion/core'

const VnzGeneral = props => {
  const { data } = props
  const d = new Date()
  const n = d.getFullYear()
  const years = n - data.registration_year

  return (
    <div style={{margin: '0'}} className="columns">
      <div className="column is-two-thirds">
        <div className="columns is-mobile">
          <div className="column is-narrow">
            <ImgWrapper>
            </ImgWrapper>
          </div>
          <div className="column">
            <h5 className="title is-6">
              {data.university_short_name} -
            </h5>
            <div>
              {`   Це ${data.university_type_name} (${data.education_type_name}) що був заснований ${years} років тому${years >= 100 ? " і є одним з найстаріших вузів України" : ''}. ${data.university_director_post} ${data.education_type_name}y, ${data.university_director_fio}. Найменування органу, до сфери управління якого належить заклад освіти: ${data.university_governance_type_name} `}
            </div>
          </div>
        </div>
        <div className="tile">
          <p>
            {data.region_name}
          </p>
        </div>
        <div>
        </div>
        <div>
        </div>
        <div>
          {data.university_phone}
        </div>
        <div>
          {data.university_email}
        </div>
        <div>
          <a href={data.university_site}>
            {data.university_site}
          </a>
        </div>
        <div>
          {data.post_index}
        </div>
      </div>
      <StatContainer className="column">
        <StatHeader css={StatDiv}>Статистика</StatHeader>

        <StatItem css={StatDiv} className="columns">
          <p className="column is-three-fifths">
            Рік заснування
          </p>
          <p className="column">
            {data.registration_year}
          </p>
        </StatItem>
        <StatItem css={StatDiv} className="columns">
          <p className="column is-three-fifths">
            Форма фінансування
          </p>
          <p className="column">
            {data.university_financing_type_name}
          </p>
        </StatItem>
        <StatItem css={StatDiv} className="columns">
          <p className="column is-three-fifths">
            ЄДРПОУ
          </p>
          <p className="column">
            {data.university_edrpou}
          </p>
        </StatItem>
      </StatContainer>
    </div>
  )
}

export default VnzGeneral

const StatContainer = s.div`
background: #f4f4f4;
padding: 0;
`
const StatHeader = s.div`
padding: 1.2rem;
font-size: 1.2rem;
font-weight: 600;
`
const StatItem = s.div`
font-weight: 500;
font-size: .9rem;
margin: 0;
margin-bottom: 0 !important;
`

const StatDiv = css`
border-bottom: 1px solid #e1e1e1;
`
const ImgWrapper = s.div`
border 1px solid #777;
height: 80px;
width: 80px;
`
