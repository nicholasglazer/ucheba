import React from "react"
// import s from '@emotion/styled'

const VnzProfessionEducators = props => {
  const { data } = props
  return (
    <div>
      <div>
        Profession Educators
      </div>
      <div>
        {data.profession_educators ? data.profession_educators.map((y, i) => {
           return (
             <div key={i} className="card">
               <div className="card-content">
                 <div>speciality_name{y.professions}</div>
                 <div>full_time_count{y.full_time_count}</div>
                 <div>part_time_count{y.part_time_count}</div>
                 <div>external_count{y.external_count}</div>
                 <div>evening_count{y.evening_count}</div>
                 <div>distance_count{y.distance_count}</div>
               </div>
             </div>
           )
        }
        ) : null}
      </div>
    </div>
  )
}

export default VnzProfessionEducators
