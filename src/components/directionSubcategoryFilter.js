import React from 'react'
import s from '@emotion/styled'

const SubcategoryFilter = props => {
  const { data, setSubcategory, selected } = props
  return (
    <div>
    {data.map((c, i) => {
      return (
        <SubcategoryCard key={i} onClick={() => setSubcategory({code: c.id, name: c.name})} className={`${selected === c.id ? 'is-active' : ''} panel-block`}>
          <div>
            {c.name}
          </div>
        </SubcategoryCard>
      )}
    )}
    </div>
  )
}

const SubcategoryCard = s.div`
cursor: pointer;
`


export default SubcategoryFilter
