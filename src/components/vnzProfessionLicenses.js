import React from "react"
/* import s from '@emotion/styled' */

const VnzLicenses = props => {
  const { data } = props
  return (
    <div>
      <div>Profession Licenses</div>
      <div>
        {data.profession_licenses ? data.profession_licenses.map((y, i) => {
           return (
             <div key={i} className="card">
               <div className="card-content">
                 {y.professions}
                 {y.license_count}
                 {y.accreditaion}
                 {y.accreditaion_expired}
               </div>
             </div>
           )
        }
        ) : null}
      </div>
    </div>
  )
}

export default VnzLicenses
