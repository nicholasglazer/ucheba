import React from 'react'
import s from '@emotion/styled'
import { css } from '@emotion/core'
import { FormattedMessage, injectIntl } from 'gatsby-plugin-intl'
import { Link } from 'gatsby'
import { FaFacebookF, FaTwitter, FaTelegramPlane } from 'react-icons/fa'
import Regions from '../components/directionRegionsFilter'

const Footer = () => {
  const social = [
    {
      name: 'Facebook',
      size: '30',
      link: 'https://www.facebook.com/uchebaa',
      color: '#3b5998',
      component: FaFacebookF
    },
    {
      name: 'Twitter',
      size: '30',
      link: 'https://twitter.com/ucheba_ua',
      color: '#1DA1F2',
      component: FaTwitter
    },
    {
      name: 'Telegramm',
      size: '30',
      link: 'https://t.me/uchebaua',
      color: '#0088cc ',
      component: FaTelegramPlane
    }
  ]
  return (
    <FooterSection className="section">
      <footer className="container">
        <Logo css={insetDeepShadow}>УЧЁБА.ЮА</Logo>
        <div className="tile is-ancester">
          <div className="tile is-vertical">
            <Slogan className="tile is-parent">
              <FormattedMessage id="footer_slogan" />
            </Slogan>
            <div>
              {/* <Regions setRegion={this.setRegion} selectedRegion={this.state.selectedRegion} /> */}
            </div>

            <AllRights css={insetDeepShadow} className="is-vertical">
              <p>
                © {new Date().getFullYear()} <a css={styledLink} href="https://ucheba.ua">ucheba.ua</a>
              </p>
              <FormattedMessage id="footer_allRights" />
            </AllRights>
          </div>
          <div style={{ paddingLeft: '4rem'}} className="tile is-vertical is-10">
            <div className="tile is-vertical">

              <MainTitle className="tile is-parent">
                <div className="tile is-child box">
                  <FormattedMessage id="footer_title"/>
                </div>
              </MainTitle>

              <div className="tile is-parent">
                <div className="tile is-child is-8">
                  <SubTitle>
                    <FormattedMessage id="footer_dialog" />
                  </SubTitle>
                  <a style={{color: '#098cc8'}} href="mailto:support@ucheba.ua">support@ucheba.ua</a>
                  <br />
                  <div>+38(067)9116696</div>
                </div>
                <div className="tile is-vertical">
                  <div className="tile is-parent is-centered" style={{paddingLeft: '5px'}}>

                    {social.map((icon, i) => {
                       return (
                         <div key={i} className="tile is-child" >
                           <a
                              href={icon.link}
                              css={{
                                color: '#888',
                                '& :hover': {
                                  color: icon.color,
                                  transition: 'color .3sec ease',
                                },
                                '> svg': {
                                  textShadow: '1px 1px 2px rgba(255,255,255, 1)',
                                }
                              }}
                           >
                             { React.createElement(icon.component, {size: icon.size}, null)}
                           </a>

                         </div>
                       )
                    })}
                  </div>
                  <div className="tile is-parent is-vertical">
                    <Link css={styledBigLink} to="/conditions">
                      <FormattedMessage id="footer_condition" />
                    </Link>
                    <br />
                    <Link css={styledBigLink} to="/policy">
                      <FormattedMessage id="footer_policy" />
                    </Link>
                  </div>
                </div>

              </div>
              <Disclamer className="tile">
                <div css={insetDeepShadow}>
                  <FormattedMessage id="footer_disclamer" />
                  <Link to="/support" css={styledLink} >
                    <FormattedMessage id="footer_disclamerSupport" />
                  </Link>
                </div>
              </Disclamer>

            </div>
          </div>
        </div>
      </footer>
    </FooterSection>
  )
}


const styledBigLink = css`
text-transform: uppercase;
letter-spacing: .05rem;
font-weight: 700;
font-size: 0.9rem;
color: #868988;
transition: color .3s ease;
text-shadow: 1px 1px 2px rgba(1,1,1, 1)
padding-bottom: 1rem;
&:hover {
color: #fff;
}
`

const styledLink = css`
font-size: 1.1rem;
transition: color .3s ease;
color: #098cc8;
text-shadow: 1px 1px 2px rgba(1,1,1, 1)
&:hover {
color: #fff;
}
`
const insetDeepShadow = css`
background-color: #767676;
color: transparent;
text-shadow: 2px 1px 3px rgba(255,255,255,0.5);
-webkit-background-clip: text !important;
background-clip: text;
letter-spacing: .05rem;
`

const FooterSection = s.section`
padding-top: 8rem;
background: #cdcdcd;
`
const Logo = s.h1`
font: bold 3rem arial, sans-serif;
padding-bottom: 7rem;
opacity: .8;
`
const Slogan = s.div`
font-size: 1.5rem;
color: white;
text-shadow: 1px 1px 3px rgba(100,100,100, .5);
font-weight: 600;
padding-left: 0 !important;
text-align: right;
`
const MainTitle = s.div`
font-size: 1.7rem;
text-shadow: 2px 2px 3px rgba(255,255,255, .3);
font-weight: 600;
padding-bottom: 10rem !important;
padding-left: 2rem;
color: #5f5f5f;
`
const SubTitle = s.div`
font-size: 1.2rem;
font-weight: 600;
color: #444;
padding-bottom: 1em;
text-transform: uppercase;
`
const AllRights = s.div`
padding-left: 0 !important;
`
const Disclamer = s.div`
letter-spacing: .06rem;
font-weight: 600;
font-size: 1.1rem;
padding-top: 4rem !important;
opacity: .8;
`

export default injectIntl(Footer)

/* const insetShadow = css
 * color: #202020;
 * letter-spacing: .1em;
 * text-shadow:
 *                      -1px -1px 1px #111,
 *       2px 2px 1px #363636; */
