import React from 'react'
import s from '@emotion/styled'
import { FormattedMessage, injectIntl } from 'gatsby-plugin-intl'
import QualificationFilter from '../components/directionQualificationFilter'
import SubcategoryFilter from '../components/directionSubcategoryFilter'
import Regions from '../components/directionRegionsFilter'
import CheckboxFilter from '../components/directionCheckboxFilter'

const FilterPanel = props => {
  const { data, resetFilter, setRegion, setQualification, setSubcategory, setEducationType, setFinancingType, selectedEducationType, selectedFinancingType, selectedQualification, selectedSubcategory, selectedRegion, filteredQuantity } = props
  return (
    <FilterPanelWrapper>
      <nav className="panel" style={{width: '280px'}}>
        <div className="panel-block" style={{borderTop: '0'}}>
          <Regions setRegion={setRegion} selectedRegion={selectedRegion} />
        </div>
          <CheckboxFilter setType={setEducationType} selectedType={selectedEducationType} />
           <p className="panel-heading" style={{fontSize: '18px'}}>
            <FormattedMessage id="direction_financingType" />
          </p>
          <CheckboxFilter setType={setFinancingType} selectedType={selectedFinancingType} />
      </nav>
    </FilterPanelWrapper>
  )
}

const FilterPanelWrapper = s.div`
margin-left: auto;
`

export default injectIntl(FilterPanel)

/* <SubcategoryFilter selected={selectedSubcategory} setSubcategory={setSubcategory} data={data}/> */
/* <QualificationFilter selected={selectedQualification}  setQualification={setQualification}/> */


/* <span style={{color: 'rgb(9,140,200)'}}>
 * {filteredQuantity}
 * </span> */
