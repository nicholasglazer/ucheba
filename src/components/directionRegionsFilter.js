import React from "react"
import s from '@emotion/styled'
import { css } from '@emotion/core'
import { MdPlace } from 'react-icons/md'

const Regions = props => {
  const { data, setRegion, selectedRegion } = props
  const regions = [
    'Місто',
    'КИЇВ',
    'Вінницька область',
    'Волинська область',
    'Дніпропетровська область',
    'Донецька область',
    'Житомирська область',
    'Закарпатська область',
    'Запорізька область',
    'Івано-Франківська область',
    'Київська область',
    'Кіровоградська область',
    'Луганська область',
    'Львівська область',
    'Миколаївська область',
    'Одеська область',
    'Полтавська область',
    'Рівненська область',
    'Сумська область',
    'Тернопільська область',
    'Харківська область',
    'Херсонська область',
    'Хмельницька область',
    'Черкаська область',
    'Чернівецька область',
    'Чернігівська область'
  ]
  const handleChange = (evt) => setRegion(evt.target.value)

  return (
      <RegionSearchStyles>
        <MdPlace size={20}/>
        <div className="select is-rounded is-small" >
          <Select value={selectedRegion} onChange={handleChange}>
            {regions.map((x, i) => <option key={i} >{x}</option>)}
          </Select>
        </div>
      </RegionSearchStyles>
  )
}

const Select = s.select`
width: 100%;
& :focus {
border: 2px solid #63b5cf !important;
box-shadow: 0px 0px 6px rgba(244,244,1, .1) !important;
}
`
const RegionSearchStyles = s.div`
display: flex;
align-items: center;
width: 100%;
& > div {
flex: 1;
}
& > svg {
fill: #666;
margin-right: 0.3rem;
}
`
export default Regions
