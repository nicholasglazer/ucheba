import React from 'react'
import s from '@emotion/styled'
import { MdArrowBack } from 'react-icons/md'

const BackHistoryButton = (props) => {
  const { size } = props
  const goBack = () => {
    window.history.back()
  }
  return (
    <W>
      <BackHistory className="navbar-item" onClick={goBack} >
        <MdArrowBack size={size} /> 
      </BackHistory>
    </W>
  )
}
export default BackHistoryButton

const W = s.div`
display: flex;
align-itmes: center;
`

const BackHistory = s.a`
padding: 0px !important;
border: 1px solid #383838;
border-radius: 4px;
text-align: center;
display: flex;
align-items: center;
justify-content: center;
width: 50px;
height: 38px;
:hover {
border: 1px solid #787878;
}
:active, :focus {
box-shadow: inset 0 0 10px rgba(0,0,0, .5)
}
`
