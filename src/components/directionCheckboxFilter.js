import React from 'react'
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl"

const CheckboxTypeFilter = (props) => {
  const { setType, selectedType } = props;
  const handleType = ({target}) => {
    const data = []
    // TODO decision: make ternary and change to reduce?
    selectedType.map(({name, checked},i) => {

      if (target.checked) {
        if (name === target.value) {
          data.push({name, checked: true})
        } else {
          data.push({name, checked})
        }
      } else {
        if (name === target.value) {
          data.push({name, checked: false})
        } else {
          data.push({name, checked})
        }
      }
    })
    setType(data)
  }
  return (
    <div className="">
      {selectedType.map(({name, checked},i) => {
         return (
           <label key={i} className="panel-block">
             <input type="checkbox" checked={checked} onChange={handleType} value={name} />
             {name}
           </label>
         )
      })}
    </div>
  );
};

export default injectIntl(CheckboxTypeFilter)
/* style={{width: '100%', justifyContent: 'space-around'}} */
