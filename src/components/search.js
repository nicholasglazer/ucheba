import React, { Component } from 'react'
import * as JsSearch from 'js-search'
import { css } from '@emotion/core'
import s from '@emotion/styled'
import { MdSearch } from 'react-icons/md'

class ClientSearch extends Component {
  state = {
    isLoading: true,
    search: null,
    isError: false,
    indexByEdrpou: false,
    indexByName: false,
    indexByShortName: false,
    termFrequency: true,
    removeStopWords: false,
    searchQuery: "",
    selectedStrategy: "",
    selectedSanitizer: "",
  }
  /**
   * React lifecycle method that will inject the data into the state.
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.search === null) {
      const { engine } = nextProps
      return {
        indexByEdrpou: engine.EdrpouIndex,
        indexByName: engine.NameIndex,
        indexByShortName: engine.ShortNameIndex,
        termFrequency: engine.SearchByTerm,
        selectedSanitizer: engine.searchSanitizer,
        selectedStrategy: engine.indexStrategy,
      }
    }
    return null
  }
  async componentDidMount() {
    this.rebuildIndex()
  }

  /**
   * rebuilds the overall index based on the options
   */
  rebuildIndex = () => {
    const {
      selectedStrategy,
      selectedSanitizer,
      removeStopWords,
      termFrequency,
      indexByEdrpou,
      indexByName,
      indexByShortName,
    } = this.state
    const { jsonData } = this.props

    const dataToSearch = new JsSearch.Search("name")

    if (removeStopWords) {
      dataToSearch.tokenizer = new JsSearch.StopWordsTokenizer(
        dataToSearch.tokenizer
      )
    }
    /**
     * defines an indexing strategy for the data
     * read more about it here https://github.com/bvaughn/js-search#configuring-the-index-strategy
     */
    if (selectedStrategy === "All") {
      dataToSearch.indexStrategy = new JsSearch.AllSubstringsIndexStrategy()
    }
    if (selectedStrategy === "Exact match") {
      dataToSearch.indexStrategy = new JsSearch.ExactWordIndexStrategy()
    }
    if (selectedStrategy === "Prefix match") {
      dataToSearch.indexStrategy = new JsSearch.PrefixIndexStrategy()
    }

    /**
     * defines the sanitizer for the search
     * to prevent some of the words from being excluded
     */
    selectedSanitizer === "Case Sensitive"
                       ? (dataToSearch.sanitizer = new JsSearch.CaseSensitiveSanitizer())
                       : (dataToSearch.sanitizer = new JsSearch.LowerCaseSanitizer())
    termFrequency === true
                       ? (dataToSearch.searchIndex = new JsSearch.TfIdfSearchIndex("id"))
                       : (dataToSearch.searchIndex = new JsSearch.UnorderedSearchIndex())

    // sets the index attribute for the data
    if (indexByEdrpou) {
      dataToSearch.addIndex("id")
    }
    // sets the index attribute for the data
    if (indexByName) {
      dataToSearch.addIndex("name")
    }

    if (indexByShortName) {
      dataToSearch.addIndex("shortName")
    }

    dataToSearch.addDocuments(jsonData) // adds the data to be searched

    this.setState({ search: dataToSearch, isLoading: false })
  }
  /**
   * handles the input change and perfom a search with js-search
   * in which the results will be added to the state
   */
  searchData = e => {
    const { search } = this.state
    const queryResult = search.search(e.target.value)
    this.setState({ searchQuery: e.target.value })
    this.props.cf(queryResult, e.target.value)
  }
  handleSubmit = e => {
    e.preventDefault()
  }
  render() {
    const { searchQuery } = this.state
    return (
      <SearchBoxSection className="section">
        <div className="container">
          <SearchBox onSubmit={this.handleSubmit}>
            <MdSearch css={searchIcon} />
            <input
              css={search}
              value={searchQuery}
              onChange={this.searchData}
              placeholder={this.props.searchInputText}
            />
          </SearchBox>
        </div>
      </SearchBoxSection>
    )
  }
}

const SearchBoxSection = s.section`
padding-bottom: 5rem;
padding-top: 0;
`
const SearchBox = s.form`
display: flex;
position: relative;
align-items: center;
transform: none;
padding: 5px;
background: white;
border-radius: 8px;
transition: transform 0.4s ease-out 0.1s;
box-shadow: 1px 1px 20px rgba(0,0,0, .24);
`
const search = css`
height: 70px;
width: 100%;
font-size: 22px;
background: #f7f7f7;
color: black;
border-radius: 8px;
outline: none;
border-style: none;
box-shadow: none;
padding-left: 75px;
@media (max-width: 767px) {
  height: 36px;
  padding-left: 44px;
  font-size: 18px;
}
`
const searchIcon = css`
position: absolute;
left: 30px;
z-index: 1;
pointer-events: none;
height: 26px;
width: 26px;
@media (max-width: 767px) {
  left: 14px;
  font-size: 18px;
}
`

export default ClientSearch
