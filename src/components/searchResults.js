import React from "react"
import { Link } from 'gatsby'
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl"
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import s from '@emotion/styled'
/* import { css } from '@emotion/core' */


const SearchResults = props => {
  const queryResults = props.queryResults
  return (
    <SearchResultsSection className="section">
      <div className="container">
        <ResultsQuantityBox>
          <ResultsQuantity>
            <FormattedMessage id="results" />
            <span style={{marginLeft: '1rem'}}>{queryResults.length}</span>
          </ResultsQuantity>
        </ResultsQuantityBox>
        <ResultsQuantityDivider>
          <FormattedMessage id="results_divider" />
        </ResultsQuantityDivider>
        {queryResults.map(item => {
           const short = item.shortName
           const full = item.name
           let linkName = short === '' || short === '.' || short === '-' ? full : short
           const link = cyrillicToTranslit({ preset: "uk" }).transform(`${linkName}`, "_").toLowerCase()
           return (
             <Link key={item.id} to={`/${link}`}>
               <UniversityBox className="media" >
                 <figure className="media-left">
                   <p className="image is-64x64">
                     <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png" alt="mock" />
                   </p>
                 </figure>
                 <div className="media-content">
                   <div className="content">
                     <p>
                       <strong>
                         {
                           item.shortName.length <= 1 ?
                           item.name :
                           item.shortName
                         }
                       </strong>
                       <br />
                       <small>
                         {item.name}
                       </small>
                     </p>
                   </div>
                   <nav className="level is-mobile">
                     <div className="level-left">
                       <div className="level-item">
                         <span className="icon is-small"><i className="fas fa-reply"></i></span>
                       </div>
                       <div className="level-item">
                         <span className="icon is-small"><i className="fas fa-heart"></i></span>
                       </div>
                     </div>
                   </nav>
                 </div>
                 <div className="media-right">
                   {item.id}
                 </div>
               </UniversityBox>
             </Link>
           )
        })}
      </div>
    </SearchResultsSection>
  )
}

/* const shadow = css({
 *   boxShadow: '1px 1px 10px black'
 * })
 *  */
/* const HoverUniversityBoxShadow = css({
 *   ':hover,:focus': shadow
 * })
 *  */
const UniversityBox = s.article`
padding: 25px 0px;
cursor: pointer;
border-bottom: 1px dashed rgba(0,0,0, .1);
`
const SearchResultsSection = s.section`
`
const ResultsQuantityBox = s.div`
display: flex;
align-items: center;
justify-content: flex-start;
margin-bottom: 1.8rem;
`
const ResultsQuantity = s.div`
font-size: 2.1rem;
font-weight: 700;
color: black;
display: flex;
`
const ResultsQuantityDivider = s.div`
background: #f1f1f9;
margin: .2rem -1.2rem;
padding: 0 1.2rem;
`
export default injectIntl(SearchResults)
