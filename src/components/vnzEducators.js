import React from "react"
import s from '@emotion/styled'
import { css } from '@emotion/core'

const VnzEducators = props => {
  const { data } = props
  return (
    <div>
      <div>
        educators
      </div>
      <div>
        {data.educators ? data.educators.map((y, i) => {
           return (
             <div key={i} className="card">
               <div className="card-content">
                 <div>qualification_group_name{y.qualification_group_name}</div>
                 <div>speciality_code{y.speciality_code}</div>
                 <div>speciality_name{y.speciality_name}</div>
                 <div>specialization_name{y.specialization_name}</div>
                 <div>full_time_count{y.full_time_count}</div>
                 <div>part_time_count{y.part_time_count}</div>
                 <div>external_count{y.external_count}</div>
                 <div>evening_count{y.evening_count}</div>
                 <div>distance_count{y.distance_count}</div>
               </div>
             </div>
           )
        }
        ) : null }
      </div>
    </div>
  )
}

export default VnzEducators
