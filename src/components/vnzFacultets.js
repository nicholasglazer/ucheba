import React from "react"
/* import s from '@emotion/styled' */

const VnzFacultets = props => {
  const { data } = props
  console.log('facultets', data)
  return (
      <div>
        {data.facultets.map((y, i) => {
           return (
             <div key={i} className="card">
               <div className="card-content">
                 {y}
               </div>
             </div>
           )
        }
        )}
      </div>
  )
}

export default VnzFacultets
