import React from 'react'
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl"

const VnzTab = (props) => {
  const { name } = props.tab;
  const { activeTab, changeActiveTab } = props;

  return (
    <li className={name === activeTab ? "is-active" : null} onClick={() => changeActiveTab(name)}>
      <a>
        <FormattedMessage id={`${name}`}/>
      </a>
    </li>
  );
};

export default injectIntl(VnzTab)
