import React from "react"
import { Link } from 'gatsby'
import { css } from '@emotion/core'

const VnzBranches = props => {
  const { data } = props
  return (
    <div className="column">
      <Link to={`/${data.fields.slug}`}>
      <div className="card" css={fullH}>
        <div className="card-content">
          <div className="media">
            <div className="media-left">
              <figure className="image is-48x48">
                <img src="https://bulma.io/images/placeholders/96x96.png" alt="mock" />
              </figure>
            </div>
            <div className="media-content">
              <p className="title is-4">{data.university_name}</p>
            </div>
          </div>
          <div className="content">
            {data.region_name}
          </div>
        </div>
      </div>
      </Link>
    </div>
  )
}

const fullH = css`
height: 100%;
`

export default VnzBranches

/* <p className="subtitle is-6">{data.education_type_name}</p> */
