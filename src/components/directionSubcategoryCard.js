import React from "react"
import { Link } from 'gatsby'
import s from '@emotion/styled'
import {css} from '@emotion/core'
import { FormattedMessage, injectIntl } from 'gatsby-plugin-intl'
import { FaCertificate } from 'react-icons/fa'
import Tooltip from '../components/tooltip'
const DirectionSubcategoryResult = props => {
  const { filteredResults } = props
  return (
    <Wrapper>
      {filteredResults.length > 0 ? filteredResults.map((x, i) => {
         let dateExpired = new Date(x.certificate_expired)
         let dateNow = Date.now()
         return (
           <Link key={i} to={`/${x.slug}`} css={linkStyle} className="subcategory-box">
             <QualificatinCard>
               <div className="">
                 {x.university_name}
               </div>
               <QCardMiddle>
                 <div className="" style={{color: '#555'}}>
                   {x.speciality_name}
                 </div>
                 <div className="" style={{color: '#444'}}>
                   {x.qualification_group_name}
                 </div>
               </QCardMiddle>
               <QCardBottom>
                 <div style={{zIndex: '100'}}>
                   <Tooltip message={`${x.certificate_expired !== null ? dateNow >= dateExpired ? 'expired at: ' : 'valid untill: ' : 'no license data'} ${x.certificate_expired === null ? '' : x.certificate_expired}`} position={'top'}>
                     <FaCertificate size={16} color={x.certificate_expired !== null ? dateNow >= dateExpired ? 'indianred' : 'forestgreen' : '#a1a1a1'}/>
                   </Tooltip>
                 </div>
                 <div>
                   {x.region_name}
                 </div>
               </QCardBottom>
             </QualificatinCard>
           </Link>
         )
      }) : (<FormattedMessage id="direction_subcategoryNotFound" />)
      }
    </Wrapper>
  )
}

const Wrapper = s.div`
padding-top: 1rem;
display: flex;
justify-content: space-between;
flex-wrap: wrap;
`
const QCardMiddle = s.div`
display: flex;
align-items: center;
justify-content: space-between;
`
const QCardBottom = s.div`
display: flex;
align-items: center;
justify-content: space-between;
`

const QualificatinCard = s.div`
font-size: .85rem;
font-weight: 500;
border-radius: 4px;
width: 100%;
flex: 1;
padding: 1rem;
background: #f4f5f6;
border: 1px solid rgba(144,144,144, .28);
display: flex;
flex-direction: column;
justify-content: space-between;
& > div {
color: #585959;
text-shadow: 1px 1px 2px rgba(254,254,254, .8)
}
`
const linkStyle = css`
flex: 1;
display: flex;
margin-bottom: 1rem;
margin-right: 1rem;
min-height: 180px;
max-height: 230px;
min-width: 340px;
`


export default injectIntl(DirectionSubcategoryResult)

/* NB good colors */
/* '#fff1f1' : '' : '#faf3fa'} */
