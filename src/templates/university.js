import React from 'react'
import { graphql } from 'gatsby'
import { injectIntl } from "gatsby-plugin-intl"
import Layout from '../components/layout'
import s from '@emotion/styled'
/* import { css } from '@emotion/core' */
import SEO from '../components/seo'
import Hero from '../components/hero'
import VnzMap from '../components/vnzMap'
import VnzCard from '../components/vnzCard'
import VnzTabs from '../components/vnzTabs'
import VnzGeneral from '../components/vnzGeneral'
import VnzFacultets from '../components/vnzFacultets'
import VnzBranches from '../components/vnzBranches'
import VnzSpecialityLicenses from '../components/vnzSpecialityLicenses'
import VnzEducators from '../components/vnzEducators'
import VnzProfessionEducators from '../components/vnzProfessionEducators'
import VnzProfessionLicenses from '../components/vnzProfessionLicenses'
import MapLeaflet from '../components/mapLeaflet'
import { MdArrowBack } from 'react-icons/md'

class University extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: 'vnz_about_vnz',
      filteredTabList: []
    }
    this.data = props.data
    this.u = props.data.universitiesJson
    this.pageContext = props.pageContext

    this.tabList = [
      {
        name: 'vnz_about_vnz',
        content: VnzGeneral,
        isData: true
      },
      {
        name: 'vnz_about_educators',
        content: VnzEducators,
        isData: !this.u.ueducators || this.u.educators.length !== 0
      },
      {
        name: 'vnz_about_facultets',
        content: VnzFacultets,
        isData: !this.u.facultets || this.u.facultets.length !== 0
      },
      {
        name: 'vnz_about_branches',
        content: VnzEducators,
        isData: !this.u.branches || this.u.branches.length !== 0
      },
      {
        name: 'vnz_about_specialityLicenses',
        content: VnzSpecialityLicenses,
        isData: !this.u.speciality_licenses || this.u.speciality_licenses.length !== 0
      },
      {
        name: 'vnz_about_professionLicenses',
        content: VnzProfessionLicenses,
        isData: !this.u.profession_licenses || this.u.profession_licenses.length !== 0
      },
      {
        name: 'vnz_about_professionEducators',
        content: VnzProfessionEducators,
        isData: !this.u.profession_educators || this.u.profession_educators.length !== 0
      }
    ]
  }

  componentDidMount() {
    this.filterTabList()
  }

  changeActiveTab(tab) {
    this.setState({ activeTab: tab })
  }

  activeTabContent(data) {
    const activeIndex = this.tabList.findIndex((tab) => {
      return tab.name === this.state.activeTab
    })
    const Component = this.tabList[activeIndex].content
    return (<Component data={data} />)
  }

  filterTabList() {
    const res = this.tabList.reduce((fil, item) => {
      if (item.isData === true) {
        const data = {
          name: item.name,
          content: item.content,
        }
        fil.push(data)
      }
      return fil
    }, [])
    this.setState({filteredTabList: res})
  }

  render() {
    const u = this.data.universitiesJson
    const { left, right } = this.pageContext
    return (
      <Layout>
        <SEO
          title={`университети`}
          description={u.university_name}
          keywords={[u.university_short_name, u.university_address]}
        />
        <Hero bg={'#f4f4f4'} />
        <HeroSection>
          <div className="section">
            <div className="container">
              <div className="columns">
                <div className="column">
                </div>
                <div className="column is-narrow">
                  <h5 className="title is-4 ">
                    {u.university_name}
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </HeroSection>
        <div className="section">
          <div className="container">
            <HeadSection className="container">
              <VnzTabs activeTab={this.state.activeTab} changeActiveTab={this.changeActiveTab.bind(this)} tabList={this.state.filteredTabList} />
              {this.activeTabContent(u)}
            </HeadSection>
            <AboutSection>
            </AboutSection>
            <div className="container" style={{overflow: 'hidden'}}>
              <MapLeaflet data={{address: u.university_address, region: u.region_name, index: u.post_index}}/>
            </div>
            <VnzMap data={u.university_address} />
            <FeaturedSection className="">
              <div className="columns">
                <VnzCard data={left} />
                <VnzCard data={right} />
              </div>
            </FeaturedSection>
          </div>
        </div>
      </Layout>
    )

  }

}

const HeroSection = s.section`
background: #f4f4f4;
padding: 2rem;
`

const HeadSection = s.section`
margin-bottom: 1rem;
border 1px solid #e1e1e1;
border-radius: 6px;
`


const AboutSection = s.section`
padding: 3rem;
margin-bottom: 1rem;
`
const FeaturedSection = s.div`
`

export const query = graphql`
  query($slug: String!) {
    universitiesJson(fields: { slug: { eq: $slug } }) {
      ...UniversitiesProps
    }
  }
`

export const allUniversitiesJsonFragment = graphql`
  fragment UniversitiesProps on UniversitiesJson {
    fields {
      slug
    }
    university_name
    university_short_name
    registration_year
    university_edrpou
    region_name
    university_parent_id
    university_financing_type_name
    university_governance_type_name
    university_type_name
    education_type_name
    university_address
    university_phone
    university_email
    university_site
    university_director_post
    university_director_fio
    post_index
    facultets
    branches {
      university_id
      university_name
      region_name
      koatuu_id
      koatuu_name
    }
    profession_educators {
      professions
      full_time_count
      part_time_count
      external_count
      evening_count
      distance_count
    }
    educators {
      qualification_group_name
      speciality_code
      speciality_name
      specialization_name
      full_time_count
      part_time_count
      external_count
      evening_count
      distance_count
    }
    speciality_licenses {
      qualification_group_name
      speciality_code
      speciality_name
      specialization_name
      full_time_count
      part_time_count
      evening_count
      certificate
      certificate_expired
    }
    profession_licenses {
      professions
      license_count
      accreditation
      accreditation_expired
    }
  }
`

export default injectIntl(University)

/* 
 * const ActiveTabContent = (props) => {
 *   const Component = props.content
 *   return (
 *     <Component data={props.data} />
 *   )
 * }
 * 
 * <ActiveTabContent data={u} key={this.state.activeTab} content={this.activeTabContent()} /> */
