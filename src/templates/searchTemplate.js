import React from 'react'
import s from '@emotion/styled'
import { css } from '@emotion/core'
import SEO from '../components/seo'
import Search from '../components/search'
import SearchResults from '../components/searchResults'
import { MdClose } from 'react-icons/md'

class UniversitiesSearchTemplate extends React.Component {
  constructor(props) {
    super(props)
    this.callback = this.callback.bind(this)
    this.state = {
      searchResults: '',
      searchQuery: '',
    }
  }

  callback(searchResults, searchQuery) {
    this.setState({searchResults, searchQuery})
  }

  goBack() {
    window.history.back()
  }
  render () {
    const { pageContext } = this.props
    const { allUniversities, options } = pageContext
    const { searchResults, searchQuery } = this.state
    const queryResults = searchQuery === "" ? allUniversities : searchResults
    return (
      <section>
        <SEO
          title={'title'}
          keywords={[`вузи`, `пошук вуз`, `вуз`]}
        />
        <Hero>
          <Search searchInputText={`шукайте все`} css={fixed} cf={this.callback} jsonData={allUniversities} engine={options} />
          <MdClose onClick={this.goBack} css={fixed} />
        </Hero>
        <SearchResults queryResults={queryResults} />
      </section>
    )
  }
}

const Hero = s.div`
left: 0px;
top: 0px;
padding: 100px 0 0 0;
`
const fixed = css`
position: fixed;
top: 130px;
right: 120px;
cursor: pointer;
font-size: 1.3rem;
`
export default UniversitiesSearchTemplate
