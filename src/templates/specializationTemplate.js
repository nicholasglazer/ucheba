import React from 'react'
import s from '@emotion/styled'
import { Link } from 'gatsby'
import SEO from "../components/seo"
import Search from '../components/search'
import Hero from '../components/hero'
import Layout from '../components/layout'
import SearchResults from '../components/searchResults'

class SpecializationTemplate extends React.Component {
  constructor(props) {
    super(props)
    /* this.callback = this.callback.bind(this) */
    /* this.state = {
     *   searchResults: '',
     *   searchQuery: '',
     * } */
    this.categoryData = props.pageContext.nodeData
    this.specializationData = props.pageContext.data[0]
  }

  callback(searchResults, searchQuery) {
    this.setState({searchResults, searchQuery})
    /* console.log(this.state.searchResults) */
  }

  render () {
    console.log('props form specializationTemplate', this.categoryData)
    /* const { pageContext } = this.props
     * const { allUniversities, options } = pageContext
     * const { searchResults, searchQuery } = this.state
     * const queryResults = searchQuery === "" ? allUniversities : searchResults */
    /* console.log(queryResults) */
    return (
      <Layout>
        <SEO
          title={`пошук коледжу`}
          keywords={[`вузи`, `пошук вуз`, `вуз`]}
        />
        <Hero bg={'rgb(255,127,80)'}>
          {/* <Search searchInputText={`пошук коледжу`} cf={this.callback} jsonData={allUniversities} engine={options} /> */}
        </Hero>
        {/* <SearchResults queryResults={queryResults} /> */}
        <div className="container">
          <div>
            {this.specializationData.name}
          </div>
          <div>
            {this.specializationData.description}
          </div>
          <div className="">
            {this.categoryData.fields.slugsArray.map((x,i) => <Link to={`/spetsializatsiya/${x}`}>{x}</Link>)}
          </div>

        </div>
      </Layout>
    )
  }
}


export default SpecializationTemplate
