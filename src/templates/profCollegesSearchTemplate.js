import React from 'react'
import s from '@emotion/styled'
import SEO from "../components/seo"
import Search from '../components/search'
import Hero from '../components/hero'
import Layout from '../components/layout'
import SearchResults from '../components/searchResults'

class ProfCollegesSearchTemplate extends React.Component {
  constructor(props) {
    super(props)
    this.callback = this.callback.bind(this)
    this.state = {
      searchResults: '',
      searchQuery: '',
    }
  }

  callback(searchResults, searchQuery) {
    this.setState({searchResults, searchQuery})
    /* console.log(this.state.searchResults) */
  }

  render () {
    const { pageContext } = this.props
    const { allUniversities, options } = pageContext
    const { searchResults, searchQuery } = this.state
    const queryResults = searchQuery === "" ? allUniversities : searchResults
    /* console.log(queryResults) */
    return (
      <Layout>
        <SEO
          title={`пошук коледжу`}
          keywords={[`вузи`, `пошук вуз`, `вуз`]}
        />
        <Hero bg={'rgb(255,127,80)'}>
          <Search searchInputText={`пошук коледжу`} cf={this.callback} jsonData={allUniversities} engine={options} />
        </Hero>
        <SearchResults queryResults={queryResults} />
      </Layout>
    )
  }
}


export default ProfCollegesSearchTemplate
