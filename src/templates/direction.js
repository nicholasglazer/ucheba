
import React from 'react'
import { graphql } from 'gatsby'
import { injectIntl } from "gatsby-plugin-intl"
import Layout from '../components/layout'
import SEO from '../components/seo'
import Hero from '../components/hero'
import s from '@emotion/styled'
import Regions from '../components/directionRegionsFilter'
import DirectionFilterPanel from '../components/directionFilterPanel'
import DirectionFilterResetButton from '../components/directionFilterResetButton'
import DirectionSubcategoryCard from '../components/directionSubcategoryCard'
import SubcategoryFilterButton from '../components/directionSubcategoryFilterButton'
import QualificationFilter from '../components/directionQualificationFilter'

class Direction extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      universities: [],
      filteredResults: [],
      selectedSubcategory: {
        name: '',
        code: ''
      },
      selectedQualification: 'Магістр',
      selectedFinancingType: [
        {
          name: 'Державна',
          checked: true,
        },
        {
          name: 'Приватна',
          checked: true,
        },
        {
          name: 'Комунальна',
          checked: true,
        }
      ],
      selectedEducationType: [
        {
          name: 'Академія',
          checked: true,
        },
        {
          name: 'Університет',
          checked: true,
        },
        {
          name: 'Інститут',
          checked: true,
        },
        {
          name: 'Коледж',
          checked: true,
        },
        {
          name: 'Технікум (училище)',
          checked: true,
        },
        {
          name: 'Відокремлений підрозділ',
          checked: true,
        },
        {
          name: 'Інша наукова установа (організація)',
          checked: true,
        }
      ],
      selectedRegion: 'Місто'
    }

    this.props = props
    this.data = props.data
    this.setQualification = this.setQualification.bind(this)
    this.setSubcategory = this.setSubcategory.bind(this)
    this.setRegion = this.setRegion.bind(this)
    this.setEducationType = this.setEducationType.bind(this)
    this.setFinancingType = this.setFinancingType.bind(this)
    this.resetFilter = this.resetFilter.bind(this)
  }

  componentDidMount() {
    this.collectUniversities()
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectedQualification !== prevState.selectedQualification
        || this.state.selectedSubcategory.code !== prevState.selectedSubcategory.code
        || this.state.selectedRegion !== prevState.selectedRegion
        || this.state.selectedEducationType !== prevState.selectedEducationType
        || this.state.selectedFinancingType !== prevState.selectedFinancingType) {

      this.filterSpec(this.state.selectedSubcategory.code, this.state.selectedQualification, this.state.selectedRegion, this.state.selectedFinancingType, this.state.selectedEducationType)
    }
  }

  collectUniversities() {
    let data = []
    this.data.allUniversitiesJson.edges.forEach(u => {
      u.node.speciality_licenses.forEach(s => {
        const c = this.createSubCategoriesMap().get(s.speciality_code)
        if (c) {
          if (c.id == s.speciality_code) {
            const w = {
              "university_name": u.node.university_name,
              "university_short_name": u.node.university_short_name,
              "speciality_code": s.speciality_code,
              "speciality_name": s.speciality_name,
              "qualification_group_name": s.qualification_group_name,
              "category_id": c.id,
              "slug": u.node.fields.slug,
              "region_name": u.node.region_name,
              "education_type_name": u.node.education_type_name,
              "financing_type_name": u.node.university_financing_type_name,
              "certificate_expired": s.certificate_expired

            }
            data.push(w)
          }
        }
      })
    })
    this.setState({selectedSubcategory: {code: data[0].speciality_code, name: data[0].speciality_name}})
    this.setState({universities: data})
  }

  createSubCategoriesMap() {
    return this.data.directionsJson.categories.reduce((map, sc) => {
      map.set(sc.id, sc)
      return map
    }, new Map())
  }

  filterSpec(scid, grade, region, fin, edu) {
    let currentlySelectedFinancingTypes = this.state.selectedFinancingType.map(type => type.checked ? type.name : false);
    let currentlySelectedEducationTypes = this.state.selectedEducationType.map(type => type.checked ? type.name : false);
    const filteredResults = this.state.universities
                                .filter(u => u.speciality_code === scid)
                                .filter(u => grade === 'Усі' || u.qualification_group_name === grade)
                                .filter(u => region === 'Місто' || u.region_name === region)
                                .filter(u => currentlySelectedFinancingTypes.includes(u.financing_type_name))
                                .filter(u => currentlySelectedEducationTypes.includes(u.education_type_name))

    this.setState({filteredResults: filteredResults})
  }

  setQualification(q) {
    this.setState({selectedQualification: q})
  }
  setSubcategory(c) {
    this.setState({selectedSubcategory: c})
  }
  setRegion(r) {
    this.setState({selectedRegion: r})
  }
  setEducationType(e) {
    this.setState({selectedEducationType: e})
  }
  setFinancingType(f) {
    this.setState({selectedFinancingType: f})
  }
  resetFilter() {
    this.setState({
      selectedRegion: 'Місто',
      selectedQualification: 'Усі',
      selectedFinancingType: [
        {
          name: 'Державна',
          checked: true,
        },
        {
          name: 'Приватна',
          checked: true,
        },
        {
          name: 'Комунальна',
          checked: true,
        }
      ],
      selectedEducationType: [
        {
          name: 'Академія',
          checked: true,
        },
        {
          name: 'Університет',
          checked: true,
        },
        {
          name: 'Інститут',
          checked: true,
        },
        {
          name: 'Коледж',
          checked: true,
        },
        {
          name: 'Технікум (училище)',
          checked: true,
        },
        {
          name: 'Відокремлений підрозділ',
          checked: true,
        },
        {
          name: 'Інша наукова установа (організація)',
          checked: true,
        }
      ]
    })
  }

  render() {
    const d = this.data.directionsJson
    return (
      <Layout>
        <SEO
          title={`напрямки`}
          description={d.category_description}
          keywords={[d.category_name]}
        />
        <Hero bg="#d1d2e4">
          <AboutCategory className="container">
            <div style={{paddingBottom: '3rem'}}>
              <b>
              {`${d.category_name.charAt(0).toUpperCase()}${d.category_name.slice(1)}`}
              </b>
              {d.category_description}</div>
          </AboutCategory>
        </Hero>
        <SubcategoryFilterButton selected={this.state.selectedSubcategory.code} setSubcategory={this.setSubcategory} data={d.categories}/>
        <div className="container" style={{paddingTop: '2rem'}}>
          <div className="tile">
            <div style={{width: '100%'}}>
              <div className="columns is-vcentered" style={{margin: '0'}}>
                <DirectionTitle className="title" style={{marginRight: `1rem`}}>
                  {this.state.selectedSubcategory.name}
                </DirectionTitle>
                <DirectionTitle className="title column" style={{borderLeft: `4px solid ${d.category_color}`}}>
                  {this.state.selectedQualification}:
                  <span style={{color: 'rgb(9,140,200)', marginLeft: '1rem'}}>
                    {this.state.filteredResults.length}
                  </span>
                </DirectionTitle>
                <DirectionFilterResetButton resetFilter={this.resetFilter} />
              </div>
              <QualificationFilter selected={this.state.selectedQualification}  setQualification={this.setQualification}/>
              <div className="tile">
                <DirectionSubcategoryCard className="tile is-8" filteredResults={this.state.filteredResults} />
                <DirectionFilterPanel
                  filteredQuantity={this.state.filteredResults.length}
                  selectedEducationType={this.state.selectedEducationType}
                  selectedFinancingType={this.state.selectedFinancingType}
                  selectedQualification={this.state.selectedQualification}
                  selectedSubcategory={this.state.selectedSubcategory.code}
                  selectedRegion={this.state.selectedRegion}
                  setEducationType={this.setEducationType}
                  setFinancingType={this.setFinancingType}
                  setQualification={this.setQualification}
                  setSubcategory={this.setSubcategory}
                  setRegion={this.setRegion}
                  resetFilter={this.resetFilter}
                  data={d.categories}
                />

              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}


const DirectionTitle = s.div`
text-transform: capitalize;
font-size: 1.6rem;
margin-top: .8rem;
`
const AboutCategory = s.div`
color: #383838;
padding-bottom: 2rem;
& > span {
text-transform: capitalize;
}
`
export const query = graphql`
  query($slug: String!) {
    directionsJson(fields: { slug: { eq: $slug } }) {
      ...DirectionsProps
    }
    allUniversitiesJson {
      edges {
        node {
          fields {
            slug
          }
          university_name
          university_short_name
          region_name
          education_type_name
          university_financing_type_name
          speciality_licenses {
            qualification_group_name
            speciality_code
            speciality_name
            certificate_expired
          }
        }
      }
    }
  }
`

export const allDirectionsJsonFragment = graphql`
  fragment DirectionsProps on DirectionsJson {
    category_id
    category_name
    category_description
    category_color
    categories {
      id
      name
      description
    }
  }
`

export default injectIntl(Direction)
