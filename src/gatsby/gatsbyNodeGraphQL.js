const gatsbyNodeGraphQL = `
universities: allUniversitiesJson {
  edges {
    node {
      fields {
        slug
      }
      university_name
      university_short_name
      university_edrpou
      education_type_name
      region_name
    }
  }
}
colleges: allCollegesJson {
  edges {
    node {
      fields {
        slug
      }
      university_name
      university_short_name
      university_edrpou
      education_type_name
      region_name
    }
  }
}
directions: allDirectionsJson {
  edges {
    node {
      fields {
        slug
        slugsArray
      }
      category_id
      category_name
      category_description
      categories {
        id
        link
        name
        description
      }
    }
  }
}
`

module.exports = gatsbyNodeGraphQL

// lang
// data {
//   title {
//     text
//   }
//   cover {
//     localFile {
//       childImageSharp {
//         resize(width: 600) {
//           src
//         }
//       }
//     }
//   }
// }
