const _ = require('lodash')

const prevNext = (list, item) => {
  // Create a random selection of the other posts (excluding the current post)
  /* const filterUnique = _.filter(list, input => input.node.fields.slug !== item.node.fields.slug && input.node.education_type_name === item.node.education_type_name) */
  const filterUnique = _.filter(list, input => input.node.fields.slug !== item.node.fields.slug && input.node.region_name === item.node.region_name)

  /* console.log("rgiorn", item.node.region_name) */

  const sample = _.sampleSize(filterUnique, 2)

  return {
    left: sample[0].node,
    right: sample[1].node,
  }
}

const createUniversities = (list, createPage, template) =>
      list.forEach(university => {
        const { left, right } = prevNext(list, university)

        const {
          fields: { slug },
        } = university.node

        /* console.log('left right:', left) */
        createPage({
          path: slug,
          component: template,
          context: {
            slug,
            left,
            right,
          },
        })
      })

const createDirections = (list, createPage, template) =>
      list.forEach(direction => {
        const {
          fields: { slug },
        } = direction.node

        createPage({
          path: slug,
          component: template,
          context: {
            slug,
          },
        })
      })

const createSpecializations = (list, createPage, template) =>
      list.forEach(direction => {
        const {
          fields: { slugsArray },
        } = direction.node
        slugsArray.forEach(slug => {
          /* console.log('s lugg: ', slug) */
          createPage({
            path:`/spetsializatsiya/${slug}`,
            component: template,
            context: {
              slug,
              nodeData: direction.node,
              data: direction.node.categories.filter(x => {
                if (slug === x.link) {
                  return x
                }
              })
            },
          })
        })
      })

const createProfCollegesSearch = (list, createPage, template) => {
  createPage({
    path: '/zaklady-professiino-technichnoi-osviti',
    component: template,
    context: {
      allColleges: list.reduce((filtered, item) => {
        const data = {
          id: item.node.university_edrpou,
          name: item.node.university_name,
          shortName: item.node.university_short_name,
        }
        filtered.push(data)
        return filtered
      }, []),
      options: {
        indexStrategy: "Prefix match",
        searchSanitizer: "Lower Case",
        NameIndex: true,
        ShortNameIndex: true,
        EdrpouIndex: true,
        SearchByTerm: true,
      },
    },
  })
}

const createCollegesSearch = (list, createPage, template) => {
  createPage({
    path: '/zaklady-vischoi-osvity_koledji',
    component: template,
    context: {
      allUniversities: list.reduce((filtered, item) => {
        if(item.node.education_type_name === "Коледж" || item.node.education_type_name === "Технікум (училище)") {
          const data = {
            id: item.node.university_edrpou,
            name: item.node.university_name,
            shortName: item.node.university_short_name,
          }
          filtered.push(data)
        }
        return filtered
      }, []),
      options: {
        indexStrategy: "Prefix match",
        searchSanitizer: "Lower Case",
        NameIndex: true,
        ShortNameIndex: true,
        EdrpouIndex: true,
        SearchByTerm: true,
      },
    },
  })
}

const createUniversitiesSearch = (list, createPage, template) => {

  createPage({
    path: '/zaklady-vischoi-osvity_universitety',
    component: template,
    context: {
      allUniversities: list.reduce((filtered, item) => {
        if(item.node.education_type_name !== "Коледж" && item.node.education_type_name !== "Технікум (училище)" ) {
          /* console.log("univ: ", item.node.education_type_name) */
          const data = {
            id: item.node.university_edrpou,
            name: item.node.university_name,
            shortName: item.node.university_short_name,
          }
          filtered.push(data)
        }
        return filtered
      }, []),
      options: {
        indexStrategy: "Prefix match",
        searchSanitizer: "Lower Case",
        NameIndex: true,
        ShortNameIndex: true,
        EdrpouIndex: true,
        SearchByTerm: true,
      },
    },
  })
}
const createSearch = (list, createPage, template) => {

  createPage({
    path: '/search',
    component: template,
    context: {
      allUniversities: list.map(item => {
        return {
          id: item.node.university_edrpou,
          name: item.node.university_name,
          shortName: item.node.university_short_name,
        }
      }),
      options: {
        indexStrategy: "Prefix match",
        searchSanitizer: "Lower Case",
        NameIndex: true,
        ShortNameIndex: true,
        EdrpouIndex: true,
        SearchByTerm: true,
      },
    },
  })
}

module.exports = { createUniversities, createDirections, createSpecializations, createUniversitiesSearch, createCollegesSearch, createProfCollegesSearch, createSearch }
